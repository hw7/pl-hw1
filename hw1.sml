(* Q1 *)
fun numOfOpenersAux (s, i) =
    if i < 0 then 0 else if String.sub (s, i) = #"(" then 1 + numOfOpenersAux (s, i - 1) else numOfOpenersAux (s, i - 1);

fun numOfOpeners s =
    numOfOpenersAux (s, size s - 1);

fun numOfClosersAux (s, i) =
    if i < 0 then 0 else if String.sub (s, i) = #")" then 1 + numOfClosersAux (s, i - 1) else numOfClosersAux (s, i - 1);

fun numOfClosers s =
    numOfClosersAux (s, size s - 1);

fun balanceAux (s, i) =
    if i < 0 then true else if numOfOpenersAux (s, i) >= numOfClosersAux (s, i) then balanceAux (s, i - 1) else false;

fun balance s =
    if numOfOpeners s = numOfClosers s then balanceAux (s, size s - 1) else false;

(* Q2 *)
fun sig1 a b c =
    if 0 = 0 then c (a, b) else b;
    
fun sig2 (a, b) c =
    if (a, c b) = (0, "0") then true else b > real(0);

fun sig3 a b c d =
    if 0 = 0 then (a b) c else (a b) c;
    
fun sig4 a b c d =
    if (c, d) = (0, 0) then c else d;

fun sig5 a b c =
    c (a b, a b);

fun sig6 () =
    0

