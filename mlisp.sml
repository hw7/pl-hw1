(* 1 *)
fun isNumberAux (s, i) =
    if i < 0 then true else if Char.isDigit (String.sub (s, i)) then isNumberAux (s, i - 1) else false;

fun isNumber s =
    isNumberAux (s, size s - 1);

(* 2 *)
fun power (a, b) =
    if b = 0 then 1 else a * power (a, b - 1);

fun atoiAux (s, i) =
    if i < 0 then 0 else (ord (String.sub (s, i)) - ord #"0") * power (10, size s - i - 1) + atoiAux (s, i - 1);
    
fun atoi s =
    if size s = 0 then 0 else atoiAux (s, size s - 1);

(* 3 *)
fun tokenizeOpeners s =
    String.concatWith " ( " (String.fields (fn c => c = #"(") s);

fun tokenizeClosers s =
    String.concatWith " ) " (String.fields (fn c => c = #")") s);

fun tokenize s =
    String.tokens (fn c => c = #" ") (tokenizeClosers (tokenizeOpeners s));
